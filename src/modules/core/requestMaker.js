import AppConstants from './appconstants';
export const httpRequest = (...args) => {
	console.log("start req");
  const callback = args.pop();
  const [ params = '' ] = args;
  fetch(AppConstants.getSearchResults(params), {
      method:'GET',
      headers:{
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer AAAAAAAAAAAAAAAAAAAAAKqIxQAAAAAAYk9tljTuELSp7VXFsqmy5taEFWU%3Dbzcmqqq5HN32inN5gythV4QBYSqAg7fg5FKF5e2MPxhDA8HcvC'
      },
  })
  .then((response) => response.text())
  .then((responseText) => {
      var responseJson = JSON.parse(responseText)
      callback(responseJson.statuses, null);
  })
  .catch((error) => {
    console.log(error);
    callback(null, error);
  });
};

// export const httpPostRequest = () => {
// 	makePostRequest();
// };


// makePostRequest = () => {
// 	console.log("here");
// 	const str = "grant_type=client_credentials"
// 	fetch(AppConstants.getAccessToken(), {
// 		method: 'POST',
// 		headers: {
// 			'Accept':'application/json',
// 			'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
// 			'Authorization': 'Basic eGZNcWh5dndFYmhsR3dxd0VESlBTcVJvcDowbWpJZjd3NXU1RnI0WGhHUEZJT3ZuUnJMREdTeU9pM0xSdlgxaXZkNEVmb2hZOEF5Uw=='
// 		},
// 		body: str
// 	})
// 	.then((response) => response.text())
//   .then((responseText) => {
//       var responseJson = JSON.parse(responseText)
//       console.log(responseJson);
//       if(!responseJson.code === 200){
//           return;
//       }
//       var data = responseJson.message;
//   })
//   .catch((error) => {
//     console.log(error);
//   });
// };