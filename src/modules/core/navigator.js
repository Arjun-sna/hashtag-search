import React, {Component} from 'react'
import {Scene, Router, Actions} from 'react-native-router-flux'
// import SearchPage from './search'
import SearchPageContainer from '../tagsearch/searchPageContainer'

class AppNavigator extends Component {
    constructor(props){
        super(props);
    }

    render () {
    	return (
				<Router scenes = {scenes} />	
			);
    }
}

const scenes = Actions.create(
      <Scene key="root" hideNavBar={true}>
        <Scene key="searchpage" component={SearchPageContainer} title="Hastag Search" initial={true} />
      </Scene>
	);

module.exports=AppNavigator;