var baseUrl = 'https://api.twitter.com/';
var consumer_key = '';
var consumer_secret = '';
let endpoints = {
    getSearchResults: function(searchTerm){
     	return baseUrl + '1.1/search/tweets.json?q=%23' + searchTerm + '&count=30';
    },
    getAccessToken: function() {
    	return baseUrl + 'oauth2/token';
    },
    getEncodedAuthToken: function() {
    	return "sample";
    }
}

module.exports=endpoints;
