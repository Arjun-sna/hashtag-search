import React, { Component } from 'react';
import {
  Text,
  TextInput,
  StyleSheet,
  View,
  ListView,
  ToolbarAndroid,
  ActivityIndicator,
  TouchableOpacity
} from 'react-native';
import TextTweet from './components/textTweet';
import TextWithMediaTweet from './components/textWithMediaTweet';

class SearchPage extends Component {
  
	constructor(prop) {
		super(prop);
    var ds = new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
    });
		this.state = {
			searchText: '',
      isLoading : false,
      dataSource: ds.cloneWithRows([])
		};
	}

  componentWillReceiveProps(nextProps) {
    if (nextProps.searchResult.length !== 0) {
      this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.searchResult),
            isLoading:false
        });
    } else {
      this.setState({
            isLoading:false
        });
    }
  }

	render () {
      var content;
      if (this.state.isLoading) {
        content = this.renderSpinner();
      } else if (this.props.searchResult.length === 0) {
        content = this.renderEmptyPlaceHolder();
      } else {
        dataSource: this.state.dataSource.cloneWithRows(this.props.searchResult);
        content = this.renderListView();
      }
	    return (
	      <View style={styles.container}>
          <ToolbarAndroid
              title='Tweet Seach'
              titleColor='#FFFFFF'
              style={styles.toolbar} />
	        <TextInput style={styles.textinput}
	        		underlineColorAndroid='rgba(0,0,0,0)'
              autoFocus={true}
              style={styles.textinput}
              onChangeText={(text) => this.setState({searchText:text})}
              placeholder='Enter hastage text to search...'
              value={this.state.searchText}
            />
            <TouchableOpacity onPress={ () => { this.setState({isLoading: true}); this.props.getSearchResults(this.state.searchText); }}>
            	<Text style={styles.button}>
			          Search
			        </Text>
            </TouchableOpacity>
            { content }
	      </View>
	    );
	}

  renderSpinner() {
    return (
      <View>
        <ActivityIndicator />
      </View>
    );
  }

  renderEmptyPlaceHolder () {
    return(
      <View>
        <Text style={styles.placeholdertext}>
          No results found
        </Text>
      </View>
    );
  }

  renderListView () {
    return(
      <ListView
          dataSource={this.state.dataSource}
          renderRow={this.renderTweet.bind(this)}
          style={styles.listView}
        />
    );
  }

  renderTweet(tweet: Object, sectionId: number, rowId: number){
        var tweetContent;
        if (tweet.entities.media) {
          postContent = <TextWithMediaTweet tweet={tweet} tweetId={rowId}/>
        } else {
          postContent = <TextTweet tweet={tweet} tweetId={rowId}/>
        }
        
        return(
            <View>
                {postContent}
            </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  toolbar: {
    backgroundColor: '#125688',
    height: 56,
  },
  button: {
  	marginLeft:10,
  	marginRight:10,
  	marginBottom:20,
  	borderRadius: 5,
  	padding: 10,
  	color: '#FFFFFF',
  	fontSize: 20,
    textAlign: 'center',
 		backgroundColor: '#125688'
  },
  textinput: {
  	margin: 10,
  	padding: 5,
  	borderStyle: 'solid',
  	borderColor: '#125688',
  	borderWidth: 2,
  	borderRadius: 5
  },
  placeholdertext: {
    flex: 1,
    textAlign: 'center'
  }
});

module.exports=SearchPage;