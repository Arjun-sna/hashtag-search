import { connect } from 'react-redux';
import Actions from './action';
import SearchPage from './searchPage';

const mapStateToProps = (state) => {
  return {
    searchResult: state.searchResult || []
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSearchResults: (searchTerm) => dispatch(Actions.loadResult(searchTerm))
  };
};

const SearchPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchPage);

export default SearchPageContainer;
