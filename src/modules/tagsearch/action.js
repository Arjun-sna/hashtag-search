import { SET_SEARCH_RESULT } from './actionTypes';
import { httpRequest, httpPostRequest } from '../core/requestMaker'
export default {
  loadResult: (searchTerm) => {
    return (dispatch) => {
      httpRequest(searchTerm, (resp, err) => {
        if (resp) {
          dispatch({
            type: SET_SEARCH_RESULT,
            searchResult: resp
          });
        } else {
          dispatch({
            type: SET_SEARCH_RESULT,
            searchResult: []
          });
        }
      });
    };
  }

  // getAccessToken: () => {
  //   return (dispatch) => {
  //     httpPostRequest((err, resp) => {

  //     });
  //   }
  // }
};
