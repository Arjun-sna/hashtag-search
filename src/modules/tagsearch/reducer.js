import {
  SET_SEARCH_RESULT
} from './actionTypes';

export default {
  searchResult: (state = [], action) => {
    switch (action.type) {
      case SET_SEARCH_RESULT:
        return action.searchResult
      default:
        return state;
    }
  }
}