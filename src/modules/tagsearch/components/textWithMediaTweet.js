import React, {Component} from 'react';
import {
  StyleSheet,
  Image,
  Text,
  View
} from 'react-native';
import TweetHeader from './tweetHeader';

class TextWithMediaTweet extends Component {
	constructor(props) {
		super(props);
	}

	render() {
    var tweet = this.props.tweet;
    console.log(tweet.entities.media[0].media_url_https);
    return(
        <View style={styles.container}>
          <TweetHeader tweet={tweet} />
          <Text style={styles.tweetContent}>{this.props.tweet.text}</Text>
          <Image source={{uri: tweet.entities.media[0].media_url_https}}
                    style={styles.media}/>
        </View>
    );

	}
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#EEEEEE',
        margin:5,
        padding:10,
        borderRadius:3,
        shadowColor:'#000000',
        shadowOpacity:0.8,
        shadowRadius:5,
        shadowOffset:{
            height:5,
            width:0
        }
    },
  tweetContent:{
      fontSize: 14,
      color: '#333333',
      marginTop: 5,
      marginBottom: 5,
      marginLeft: 10,
      marginRight: 10,
  },
  media: {
    height: 300
  }
});

module.exports = TextWithMediaTweet;
