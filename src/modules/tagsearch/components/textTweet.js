import React, {Component} from 'react';
import {
  StyleSheet,
  Image,
  Animated,
  ListView,
  Text,
  View
} from 'react-native';
import TweetHeader from './tweetHeader';

class TextTweet extends Component {
	constructor(props) {
		super(props);
	}

	render() {
    var tweet = this.props.tweet;
    return(
        <View style={styles.container}>
          <TweetHeader tweet={tweet} />
          <Text style={styles.tweetContent}>{this.props.tweet.text}</Text>
        </View>
    );

	}
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#EEEEEE',
        margin:5,
        padding:10,
        borderRadius:3,
        shadowColor:'#000000',
        shadowOpacity:0.8,
        shadowRadius:5,
        shadowOffset:{
            height:5,
            width:0
        }
    },
  tweetContent:{
      fontSize: 14,
      color: '#333333',
      marginTop: 5,
      marginBottom: 5,
      marginLeft: 10,
      marginRight: 10,
  },
  thumbnail: {
    width: 40,
    height: 40,
    borderRadius:5
  }
});

module.exports = TextTweet;
