import React, {Component} from 'react'

import {
  StyleSheet,
  Image,
  Animated,
  Text,
  View
} from 'react-native';
class FeedHeader extends React.Component{
    constructor(props){
        super(props);
    }


    render(){
        var tweet = this.props.tweet;
        var date = new Date(tweet.created_at);
        var timeStamp = date.toDateString();
        return(
            <View style={styles.header_container}>
                <Image source={{uri: tweet.user.profile_image_url_https}}
                    style={styles.thumbnail}/>
                <View style={styles.details_container}>
                    <Text style={styles.tweetName}>{tweet.user.name}</Text>
                    <Text style={styles.timeStamp}>{ timeStamp }</Text>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
  header_container: {
    flex: 1,
    marginRight:10,
    marginLeft:10,
    marginTop:10,
    flexDirection: 'row',
  },
  details_container: {
    flex: 1,
    marginTop: 2,
    marginBottom: 2,
    marginLeft: 5,
    marginRight: 5
  },
  tweetName: {
    fontSize: 14,
    fontWeight:'bold',
    color: '#125688'
  },
  timeStamp:{
      fontSize: 12,
  },
  thumbnail: {
    width: 40,
    height: 40,
    borderRadius:5
  },
});

module.exports = FeedHeader;
