import React from 'react';
import { Component } from 'react-native';
import { Provider } from 'react-redux';
import AppNavigator from './modules/core/navigator';
import appReducer from './modules/core/reducer';
import thunk from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { assign } from 'lodash';

const reducers = assign({}, appReducer, {});

const initialState = {
  searchResult: []
};


const store = createStore(
  combineReducers(reducers),
  initialState,
  compose(
    applyMiddleware(thunk)
  )
);


const Main = () => {
  return (
		<Provider store={ store }>
			<AppNavigator />
		</Provider>
  )
};

export default Main;

