import React, { Component } from 'react';
import App from './src'
import {
  AppRegistry
} from 'react-native';

AppRegistry.registerComponent('HastagSearch', () => App);
